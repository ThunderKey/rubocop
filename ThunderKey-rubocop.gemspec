Gem::Specification.new do |spec|
  spec.name          = 'ThunderKey-rubocop'
  spec.version       = '0.0.31'
  spec.authors       = ['Nicolas Ganz']
  spec.email         = ['nicolas@keltec.ch']

  spec.summary       = %q{rubocop configurations of ThunderKey}
  spec.homepage      = 'https://gitlab.com/ThunderKey/rubocop'
  spec.license       = 'MIT'

  spec.files         = %w(rubocop.yml rubocop-rails.yml)

  spec.add_runtime_dependency 'rubocop', '>= 1.11.0', '< 2'
  spec.add_runtime_dependency 'rubocop-rspec', '>= 2.2.0', '< 3'
  spec.add_runtime_dependency 'rubocop-performance', '>= 1.10.0', '< 2'
  spec.add_runtime_dependency 'rubocop-rails', '>= 2.9.1', '< 3'
  spec.add_runtime_dependency 'rubocop-i18n', '>= 3.0.0', '< 4'
end
