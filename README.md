# ThunderKey rubocop configuration

[![Gem Version](https://badge.fury.io/rb/ThunderKey-rubocop.svg)](https://badge.fury.io/rb/ThunderKey-rubocop)

This gem contains the rubocop configurations for ThunderKey.

## Usage

Add the gem:

```ruby
gem 'ThunderKey-rubocop'
```

It is not necessary to require `rubocop` itself.

Create the following `.rubocop.yml` file:

```yaml
inherit_gem:
  ThunderKey-rubocop: rubocop.yml
```

Optionally add the following to merge all `Exclude` configurations:

```yaml
inherit_mode:
  merge:
    - Exclude
```